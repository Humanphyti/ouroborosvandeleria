﻿using MoreMountains.TopDownEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.TopDownEngine {
/// <summary>
/// Add this class to a character or object with a Health class, and its health will auto refill based on the settings here
/// </summary>
[AddComponentMenu("Assets/VandaleriaAssets/Scripts/MagicPower/Auto Refill")]
    public class AutoRefill : HealthAutoRefill
    {
        [Header("MP Cooldown")]
        /// how much time, in seconds, should pass before the refill kicks in
        [Tooltip("how much time, in seconds, should pass before the refill kicks in")]
        public float CooldownAfterDeplete = 2.0f;

        [Header("MP Refill Settings")]
        /// if this is true, health will refill itself when not at full health
        [Tooltip("if this is true, health will refill itself when not at full health")]
        public bool RefillMP = true;
        /// the amount of health per second to restore when in linear mode
        [Tooltip("the amount of health per second to restore when in linear mode")]
        public float MPPerSecond;
        
        protected MagicPower _mp;
        protected float _lastDepleteTime = 0f;
        protected float _mpToGive = 0f;

        /// <summary>
        /// On Awake we do our init
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            Initialization();
        }

        /// <summary>
        /// On init we grab our Health component
        /// </summary>
        protected override void Initialization()
        {
            base.Initialization();
            _mp = this.gameObject.GetComponent<MagicPower>();
            RefillHealth = false;
        }

        /// <summary>
        /// On Update we refill
        /// </summary>
        protected override void Update()
        {
            base.Update();
            ProcessRefillMP();
        }

        /// <summary>
        /// Tests if a refill is needed and processes it
        /// </summary>
        protected virtual void ProcessRefillMP()
        {
            if (!RefillMP)
            {
                return;
            }

            if (Time.time - _lastDepleteTime < CooldownAfterDeplete)
            {
                return;
            }

            _mp.MPConsumptionEnabled();
            _mpToGive += MPPerSecond * Time.deltaTime;
            if (_mpToGive > 1f)
            {
                int givenMP = (int)_mpToGive;
                _mpToGive -= givenMP;
                _mp.GetMP(givenMP, this.gameObject);
            }
        }
    }
}
