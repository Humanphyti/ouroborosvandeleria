﻿using MoreMountains.TopDownEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEquippableHandler : CharacterAbility
{
    [Header("Equippable")]
    public Equippable InitialEquippable;
    /// if this is set to true, the character can pick up PickableWeapons
    [Tooltip("if this is set to true, the character can pick up PickableEquippable")]
    public bool CanPickupEquippable = true;

    [Header("Debug")]

    /// the weapon currently equipped by the Character
    [MMReadOnly]
    [Tooltip("the weapon currently equipped by the Character")]
    public Equippable CurrentEquippable;
    /// the position the weapon will be attached to. If left blank, will be this.transform.
    [Tooltip("the position the weapon will be attached to. If left blank, will be this.transform.")]
    public Transform EquippableAttachment;

    // Initialization
    protected override void Initialization()
    {
        base.Initialization();
        Setup();
    }

    public virtual void Setup()
    {
        _character = this.gameObject.MMGetComponentNoAlloc<Character>();
        // we set the initial Armor
        if (InitialEquippable != null)
        {
            ChangeEquippable(InitialEquippable, InitialEquippable.EquippableName);
        }
    }

    /// <summary>
    /// Changes the character's current Armor to the one passed as a parameter
    /// </summary>
    /// <param name="newEquippable">The new Armor.</param>
    public virtual void ChangeEquippable(Equippable newEquippable, string EquippableID)
    {
        if (newEquippable != null)
        {
            InstantiateEquippable(newEquippable, EquippableID);
        }
        else
        {
            CurrentEquippable = null;
        }
    }

    protected virtual void InstantiateEquippable(Equippable newEquippable, string EquippableID)
    {
        CurrentEquippable = (Equippable)Instantiate(newEquippable, EquippableAttachment.transform.position + newEquippable.EquippableAttachmentOffset, EquippableAttachment.transform.rotation);

        CurrentEquippable.transform.parent = EquippableAttachment.transform;
        CurrentEquippable.transform.localPosition = newEquippable.EquippableAttachmentOffset;
        CurrentEquippable.SetOwner(_character, this);
        CurrentEquippable.EquippableID = EquippableID;

        CurrentEquippable.Initialization();
    }
}
