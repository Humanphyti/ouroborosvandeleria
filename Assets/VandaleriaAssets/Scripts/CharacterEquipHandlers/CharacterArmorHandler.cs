﻿using MoreMountains.TopDownEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterArmorHandler : CharacterAbility
{
    public override string HelpBoxText() { return "This component will allow your character to pickup and use Armor. What the Armor will do is defined in the Armor classes. This just describes the behaviour of the 'Body' holding the Armor, not the armor itself. Here you can set an initial armor for your character to start with, allow armor pickup, and specify an armor attachment (a transform inside of your character, could be just an empty child gameobject, or a subpart of your model."; }

    [Header("Armor")]
    /// the initial weapon owned by the character
    [Tooltip("the initial weapon owned by the character")]
    public Armor InitialArmor;
    /// if this is set to true, the character can pick up PickableWeapons
    [Tooltip("if this is set to true, the character can pick up PickableArmor")]
    public bool CanPickupArmor = true;

    [Header("Binding")]
    /// the position the weapon will be attached to. If left blank, will be this.transform.
    [Tooltip("the position the weapon will be attached to. If left blank, will be this.transform.")]
    public Transform ArmorAttachment;
    /// if this is true this animator will be automatically bound to the weapon
    [Tooltip("if this is true this animator will be automatically bound to the weapon")]
    public bool AutomaticallyBindAnimator = false;
    /// the ID of the AmmoDisplay this ability should update
    //[Tooltip("the ID of the AmmoDisplay this ability should update")]
    //public int AmmoDisplayID = 0;



    [Header("Debug")]

    /// the weapon currently equipped by the Character
    [MMReadOnly]
    [Tooltip("the weapon currently equipped by the Character")]
    public Armor CurrentArmor;


    /// an animator to update when the weapon is used
    public Animator CharacterAnimator { get; set; }

    protected Transform _torsoTarget = null;

    /// <summary>
    /// Sets the weapon attachment
    /// </summary>
    protected override void PreInitialization()
    {
        base.PreInitialization();
        // filler if the WeaponAttachment has not been set
        if (ArmorAttachment == null)
        {
            ArmorAttachment = transform;
        }
    }

    // Initialization
    protected override void Initialization()
    {
        base.Initialization();
        Setup();
    }

    public virtual void Setup()
    {
        _character = this.gameObject.MMGetComponentNoAlloc<Character>();
        // filler if the WeaponAttachment has not been set
        if (ArmorAttachment == null)
        {
            ArmorAttachment = transform;
        }
        // we set the initial Armor
        if (InitialArmor != null)
        {
            ChangeArmor(InitialArmor, InitialArmor.ArmorName);
        }
    }





    /// <summary>
    /// Changes the character's current Armor to the one passed as a parameter
    /// </summary>
    /// <param name="newArmor">The new Armor.</param>
    public virtual void ChangeArmor(Armor newArmor, string ArmorID)
    {
        if (newArmor != null)
        {
            InstantiateArmor(newArmor, ArmorID);
        }
        else
        {
            CurrentArmor = null;
        }
    }

    protected virtual void InstantiateArmor(Armor newArmor, string ArmorID)
    {
        CurrentArmor = (Armor)Instantiate(newArmor, ArmorAttachment.transform.position + newArmor.ArmorAttachmentOffset, ArmorAttachment.transform.rotation);

        CurrentArmor.transform.parent = ArmorAttachment.transform;
        CurrentArmor.transform.localPosition = newArmor.ArmorAttachmentOffset;
        CurrentArmor.SetOwner(_character, this);
        CurrentArmor.ArmorID = ArmorID;

        CurrentArmor.Initialization();
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        if (CurrentArmor != null)
        {
            ChangeArmor(null, "");
        }
    }

    protected override void OnRespawn()
    {
        base.OnRespawn();
        Setup();
    }
}
