﻿using MoreMountains.TopDownEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpellHandler : CharacterHandleWeapon
{
    public override string HelpBoxText() { return "This component will allow your character to pickup and use Armor. What the Armor will do is defined in the Armor classes. This just describes the behaviour of the 'Body' holding the Armor, not the armor itself. Here you can set an initial armor for your character to start with, allow armor pickup, and specify an armor attachment (a transform inside of your character, could be just an empty child gameobject, or a subpart of your model."; }

    [Header("Spell")]
    /// the initial weapon owned by the character
    [Tooltip("the initial spell owned by the character")]
    public ProjectileWeapon InitialSpell;
    /// if this is set to true, the character can pick up PickableWeapons
    [Tooltip("if this is set to true, the character can pick up PickableSpell")]
    public bool CanPickupSpells = true;

    [Header("Binding")]
    /// the position the weapon will be attached to. If left blank, will be this.transform.
    [Tooltip("the position the weapon will be attached to. If left blank, will be this.transform.")]
    public Transform SpellAttachment;

    /// the weapon currently equipped by the Character
    [MMReadOnly]
    [Tooltip("the spell currently equipped by the Character")]
    public Weapon CurrentSpell;

    protected const string _spellEquippedAnimationParameterName = "SpellEquipped";
    protected const string _spellEquippedIDAnimationParameterName = "SpellEquippedID";
    protected int _spellEquippedAnimationParameter;
    protected int _spellEquippedIDAnimationParameter;

    public override void Setup()
    {
        base.Setup();
        // filler if the WeaponAttachment has not been set
        if (SpellAttachment == null)
        {
            SpellAttachment = transform;
        }
        // we set the initial weapon
        if (InitialSpell != null)
        {
            ChangeWeapon(InitialSpell, InitialSpell.WeaponName, false);
        }
    }

}
