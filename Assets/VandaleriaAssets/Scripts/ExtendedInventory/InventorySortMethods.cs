﻿using MoreMountains.InventoryEngine;
using MoreMountains.TopDownEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySortMethods : MonoBehaviour
{
    // Gather a reference to the inventory that will be sorted
    //public Inventory _inventory;

    // Gather a reference to the inventory display to redraw where items are based on how the inventory is sorted
    //public InventoryDisplay _inventoryDisplay;

    //public InventoryInputManager _inventoryInputManager;

    public virtual void Awake()
    {
        //_inventory = GetComponent<Inventory>();
        //_inventoryDisplay = GetComponent<InventoryDisplay>();
        //_inventoryInputManager = GetComponent<InventoryInputManager>();        
    }

    ///<summary>
    /// sort the inventory by Name
    /// </summary>
    /// <param name="inventory"></param>
    public virtual void SortAlphabetically(Inventory inventory)
    {

    }

    ///<summary>
    /// sort the inventory by SellPrice
    /// </summary>
    /// <param name="inventory"></param>
    public virtual void SortSellPrice(Inventory inventory)
    {

    }

    ///<summary>
    /// sort the inventory based on the appropriate attribute
    /// </summary>
    /// <param name="inventory"></param>
    /// <param name="itemsType"></param>
    public virtual void SortStatAttribute(Inventory inventory, ItemClasses itemsType)
    {

    }


    /// <summary>
    /// call the appropriate stat attriubute sort function as in sorting on armor amount for armor, damage for weapons, etc...
    /// </summary>
    /// <param name="inventory"></param>
    /// <param name="invenType"></param>
    public virtual void PickStatAttribute(Inventory inventory, ItemClasses itemsType)
    {
        switch (itemsType)
        {
            case ItemClasses.Armor:

                break;

            case ItemClasses.Weapon:

                break;

            case ItemClasses.Spells:

                break;

            case ItemClasses.Neutral:

                break;

            case ItemClasses.Relic:

                break;

            case ItemClasses.KeyItem:

                break;

            case ItemClasses.Equippable:

                break;

            default:
                SortSellPrice(inventory);
                break;
        }
    }        
}
