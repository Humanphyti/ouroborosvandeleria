﻿using MoreMountains.InventoryEngine;
using MoreMountains.MMInterface;
using MoreMountains.Tools;
using UnityEngine;

public class CarouselInputSansButtons : InventoryInputManager
{
    [Header("Key Mapping")]
    [MMInformation("Here you need to set the various key bindings you prefer. There are some by default but feel free to change them.", MMInformationAttribute.InformationType.Info, false)]
    /// the key used to cycle the carousel right
    public string MoveCarouselRightKey = "e";
    /// the alternate key used to cycle the carousel right
    public string AltMoveCarouselRightKey = "t";
    /// the key used to cycle the carousel left
    public string MoveCarouselLeftKey = "q";
    /// the alternate key used to cycle the carousel right
    public string AltMoveCarouselLeftKey = "y";

    public MMCarousel carousel;

    protected override void Update()
    {
        base.Update();
        HandleCarouselInput();
    }

    protected virtual void HandleCarouselInput()
    {
        if (Input.GetKeyDown(MoveCarouselRightKey) || Input.GetKeyDown(AltMoveCarouselRightKey))
        {
            if (carousel.CanMoveRight())
            {
                carousel.MoveRight();
                /*if (_currentInventoryDisplay.GoToInventory(1) != null)
                {
                    _currentInventoryDisplay = _currentInventoryDisplay.GoToInventory(1);
                }*/
            }
        }

        if (Input.GetKeyDown(MoveCarouselLeftKey) || Input.GetKeyDown(AltMoveCarouselLeftKey))
        {
            if (carousel.CanMoveLeft())
            {
                carousel.MoveLeft();
            }
        }
    }
}
