﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using UnityEngine;


namespace MoreMountains.TopDownEngine
{
    /// <summary>
	/// This class manages the MagicPower of an object, pilots its potential MagicPower bar, handles what happens when it takes damage,
	/// and what happens when it dies.
	/// </summary>
	[AddComponentMenu("Assets/VandaleriaAssets/Scripts/Core/MagicPower")]
    public class MagicPower : Health
    {
        //[Header("Bindings")]

        /// the model to disable (if set so)
		//[Tooltip("the model to disable (if set so)")]
        //public GameObject Model;

		[Header("MP Status")]

		/// the current MagicPower of the character
		[MMReadOnly]
		[Tooltip("the current MagicPower of the character")]
		public int CurrentMP;
		/// If this is true, this object can't take damage
		[MMReadOnly]
		[Tooltip("If this is true, this object can't take damage")]
		public bool InfiniteMP = false;

		[Header("Magic Power")]

		[MMInformation("Add this component to an object and it'll have MagicPower, MP, and will be able to cast spells.", MoreMountains.Tools.MMInformationAttribute.InformationType.Info, false)]
		/// the initial amount of MagicPower of the object
		[Tooltip("the initial amount of MagicPower of the object")]
		public int InitialMP = 10;
		/// the maximum amount of MagicPower of the object
		[Tooltip("the maximum amount of MagicPower of the object")]
		public int MaximumMP = 10;

		[Header("Magic Feedback")]
		/// the feedback to play when reducing MP
		[Tooltip("the feedback to play when reducing MP")]
		public MMFeedbacks mpConsumeMMFeedbacks;
		/// the feedback to play when dying
		[Tooltip("the feedback to play when depleting")]
		public MMFeedbacks DepleteMMFeedbacks;

		[Header("Armor Status")]

		/// the current MagicPower of the character
		[MMReadOnly]
		[Tooltip("the current MagicPower of the character")]
		public int CurrentArmor;

		[Header("Armor")]

		[MMInformation("Add this component to an object and it'll have MagicPower, MP, and will be able to cast spells.", MoreMountains.Tools.MMInformationAttribute.InformationType.Info, false)]
		/// the initial amount of MagicPower of the object
		[Tooltip("the initial amount of MagicPower of the object")]
		public int InitialArmor = 0;
		/// the maximum amount of MagicPower of the object
		[Tooltip("the maximum amount of MagicPower of the object")]
		public int MaximumArmor = 0;

		/*protected Vector3 _initialPosition;
		protected Renderer _renderer;
		protected Character _character;
		protected TopDownController _controller;
		
		protected CharacterController _characterController;
		protected bool _initialized = false;
		protected Color _initialColor;
		protected AutoRespawn _autoRespawn;
		protected Animator _animator;
		protected int _initialLayer;*/
		protected MMHealthBar _mpBar;

		// respawn delegate
		public delegate void OnReplenishDelegate();
		public OnReplenishDelegate OnReplenish;

		// death delegate
		public delegate void OnDepleteDelegate();
		public OnDepleteDelegate OnDeplete;

		/// <summary>
		/// On Start, we initialize our MP
		/// </summary>
		protected override void Awake()
		{
			Initialization();
		}

		/// <summary>
		/// Grabs useful components, enables damage and gets the inital color
		/// </summary>
		protected override void Initialization()
		{
			_character = GetComponent<Character>();
			if (Model != null)
			{
				Model.SetActive(true);
			}

			if (gameObject.MMGetComponentNoAlloc<Renderer>() != null)
			{
				_renderer = GetComponent<Renderer>();
			}
			if (_character != null)
			{
				if (_character.CharacterModel != null)
				{
					if (_character.CharacterModel.GetComponentInChildren<Renderer>() != null)
					{
						_renderer = _character.CharacterModel.GetComponentInChildren<Renderer>();
					}
				}
			}
			if (_renderer != null)
			{
				if (_renderer.material.HasProperty("_Color"))
				{
					_initialColor = _renderer.material.color;
				}
			}

			// we grab our animator
			if (_character != null)
			{
				if (_character.CharacterAnimator != null)
				{
					_animator = _character.CharacterAnimator;
				}
				else
				{
					_animator = GetComponent<Animator>();
				}
			}
			else
			{
				_animator = GetComponent<Animator>();
			}

			if (_animator != null)
			{
				_animator.logWarnings = false;
			}

			_initialLayer = gameObject.layer;

			_autoRespawn = GetComponent<AutoRespawn>();
			_mpBar = GetComponent<MMHealthBar>();
			_controller = GetComponent<TopDownController>();
			_characterController = GetComponent<CharacterController>();

			_initialPosition = transform.position;
			_initialized = true;
			CurrentMP = InitialMP;
			MPConsumptionEnabled();
			UpdateMPBar(false);
		}

		/// <summary>
		/// When the object is enabled (on respawn for example), we restore its initial health levels
		/// </summary>
		protected override void OnEnable()
		{
			CurrentMP = InitialMP;
			if (Model != null)
			{
				Model.SetActive(true);
			}
			MPConsumptionEnabled();
			UpdateMPBar(false);
		}

		public virtual void ConsumeMP(int mpConsumed, GameObject instigator, float flickerDuration, float infiniteMPDuration)
		{
			// if the object is invulnerable, we do nothing and exit
			if (InfiniteMP)
			{
				return;
			}

			// if we're already below zero, we do nothing and exit
			if ((CurrentMP <= 0) && (InitialMP != 0))
			{
				return;
			}

			// we decrease the character's MP by the damage
			float previousMP = CurrentMP;
			CurrentMP -= mpConsumed;

			if (CurrentMP < 0)
			{
				CurrentMP = 0;
			}

			// we prevent the character from colliding with Projectiles, Player and Enemies
			//if (infiniteMPDuration > 0)
			//{
			//	MPConsumptionDisabled();
			//	StartCoroutine(MPConsumptionEnabled(infiniteMPDuration));
			//}

			// we trigger a damage taken event
			MMDamageTakenEvent.Trigger(_character, instigator, CurrentMP, mpConsumed, previousMP);

			if (_animator != null)
			{
				_animator.SetTrigger("ConsumeMP");
			}

			mpConsumeMMFeedbacks?.PlayFeedbacks(this.transform.position);

			// we update the MP bar
			UpdateMPBar(true);

			// if health has reached zero
			if (CurrentMP <= 0)
			{
				// we set its health to zero (useful for the healthbar)
				CurrentMP = 0;

				Deplete();
			}
		}

		/// <summary>
		/// Depletes the character, vibrates the device, instantiates depleted effects, etc
		/// </summary>
		public virtual void Deplete()
		{
			if (_character != null)
			{
				// we set its depleted state to true
				//if this breaks re-add the state in CharacterStates
				_character.ConditionState.ChangeState(CharacterStates.CharacterConditions.Depleted);
				
			}
			CurrentMP = 0;

			MPConsumptionDisabled();

			DepleteMMFeedbacks?.PlayFeedbacks(this.transform.position);

			// Adds points if needed.
			/*if (PointsWhenDestroyed != 0)
			{
				// we send a new points event for the GameManager to catch (and other classes that may listen to it too)
				TopDownEnginePointEvent.Trigger(PointsMethods.Add, PointsWhenDestroyed);
			}*/

			if (_animator != null)
			{
				_animator.SetTrigger("Deplete");
			}

			OnDeplete?.Invoke();
		}

		/// <summary>
		/// Called when the character gets MP (from a stimpack for example)
		/// </summary>
		/// <param name="magic">The MP the character gets.</param>
		/// <param name="instigator">The thing that gives the character MP.</param>
		public virtual void GetMP(int magic, GameObject instigator)
		{
			// this function adds MP to the character's MP and prevents it to go above MaxMP.
			CurrentMP = Mathf.Min(CurrentMP + magic, MaximumMP);
			UpdateMPBar(true);
		}

		/// <summary>
		/// Resets the character's MP to its max value
		/// </summary>
		public virtual void ResetMPToMaxMP()
		{
			CurrentMP = MaximumMP;
			UpdateMPBar(false);
		}

		/// <summary>
		/// Sets the current MP to the specified new value, and updates the MP bar
		/// </summary>
		/// <param name="newValue"></param>
		public virtual void SetMP(int newValue)
		{
			CurrentMP = newValue;
			UpdateMPBar(false);
		}

		///<summary>
		///called then the character get Armor from armor
		/// </summary>
		/// <param name="armor">The Armor the character gets</param>
		/// <param name="instigator">The thing that gives the character Armor</param>
		public virtual void GetArmor(int armor, GameObject instigator)
        {
			CurrentArmor = Mathf.Min(CurrentMP + armor, MaximumArmor);
        }

		///<summary>
		/// Resets the characters armor to its max value
		/// </summary>
		public virtual void ResetArmorToMaxArmor()
        {
			CurrentArmor = MaximumArmor;
        }

		///<summary>
		/// Sets the current Armor to the specified new value
		/// </summary>
		/// <param name="newValue"></param>
		public virtual void SetArmor(int newValue)
        {
			CurrentArmor = newValue;
        }

		/// <summary>
		/// Updates the character's MP bar progress.
		/// </summary>
		protected virtual void UpdateMPBar(bool show)
		{
			if (_mpBar != null)
			{
				_mpBar.UpdateBar(CurrentMP, 0f, MaximumMP, show);
			}

			if (_character != null)
			{
				if (_character.CharacterType == Character.CharacterTypes.Player)
				{
					// We update the MP bar
					if (GUIManager.Instance != null)
					{
						GUIManager.Instance.UpdateHealthBar(CurrentMP, 0f, MaximumMP, _character.PlayerID);
					}
				}
			}
		}

		/// <summary>
		/// Prevents the character from consuming MP
		/// </summary>
		public virtual void MPConsumptionDisabled()
		{
			InfiniteMP = true;
		}

		/// <summary>
		/// Allows the character to consume MP
		/// </summary>
		public virtual void MPConsumptionEnabled()
		{
			InfiniteMP = false;
		}

		/// <summary>
		/// makes the character able to take damage again after the specified delay
		/// </summary>
		/// <returns>The layer collision.</returns>
		/*public virtual IEnumerator MPConsumptionEnabled(float delay)
		{
			yield return new WaitForSeconds(delay);
			InfiniteMP = false;
		}*/
	}
}

