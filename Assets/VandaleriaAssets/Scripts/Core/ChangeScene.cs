﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;

    /// <summary>
    /// Demo class to go from one level to another
    /// </summary>
    public class ChangeScene : MonoBehaviour
    {
        //In changing scene I need to save all the inventories and then load them back in when the new scene loads in
        //Also need to save the current state of the inventory GUI across scenes by saving the main character prefab
        /// <summary>
		/// The name of the level to go to when entering the ChangeLevel zone
		/// </summary>
		[MMInformation("This demo component, when added to a BoxCollider2D, will change the scene to the one specified in the field below when the character enters the collider.", MMInformationAttribute.InformationType.Info, false)]
        public string Destination;

        public virtual void OnTriggerEnter2D(Collider2D collider)
        {
            if ((Destination != null) && (collider.gameObject.GetComponent<AIBrain>() == null))
            {
                MMGameEvent.Trigger("Save");
                SceneManager.LoadScene(Destination);
            }
        }
    }


/// This class handles event management, and can be used to broadcast events throughout the game, to tell one class (or many) that something's happened.
/// Events are structs, you can define any kind of events you want. This manager comes with MMGameEvents, which are 
/// basically just made of a string, but you can work with more complex ones if you want.
/// 
/// To trigger a new event, from anywhere, do YOUR_EVENT.Trigger(YOUR_PARAMETERS)
/// So MMGameEvent.Trigger("Save"); for example will trigger a Save MMGameEvent
/// 
/// you can also call MMEventManager.TriggerEvent(YOUR_EVENT);
/// For example : MMEventManager.TriggerEvent(new MMGameEvent("GameStart")); will broadcast an MMGameEvent named GameStart to all listeners.
///
/// To start listening to an event from any class, there are 3 things you must do : 
///
/// 1 - tell that your class implements the MMEventListener interface for that kind of event.
/// For example: public class GUIManager : Singleton<GUIManager>, MMEventListener<MMGameEvent>
/// You can have more than one of these (one per event type).
///
/// 2 - On Enable and Disable, respectively start and stop listening to the event :
/// void OnEnable()
/// {
/// 	this.MMEventStartListening<MMGameEvent>();
/// }
/// void OnDisable()
/// {
/// 	this.MMEventStopListening<MMGameEvent>();
/// }
/// 
/// 3 - Implement the MMEventListener interface for that event. For example :
/// public void OnMMEvent(MMGameEvent gameEvent)
/// {
/// 	if (gameEvent.EventName == "GameOver")
///		{
///			// DO SOMETHING
///		}
/// } 
/// will catch all events of type MMGameEvent emitted from anywhere in the game, and do something if it's named GameOver