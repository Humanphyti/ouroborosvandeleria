﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
    public CanvasGroup CurrentCanvasGroup;
    public CanvasGroup DestinationCanvasGroup;
    public virtual void SwitchActiveCanvasGroup()
    {
        CurrentCanvasGroup.alpha = 0;
        DestinationCanvasGroup.alpha = 1;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            SwitchActiveCanvasGroup();
        }
    }
}
