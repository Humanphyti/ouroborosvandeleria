﻿using MoreMountains.TopDownEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spells
{
    public float magicCost;
    public bool castingTime;
    public List<Animator> animators;

    protected BoxCollider2D _boxCollider2D;
    protected CircleCollider2D _circleCollider2D;


    public Spells(string name, float magicCost, bool castingTime)
    {
        //this.name = name;
        this.magicCost = magicCost;
        this.castingTime = castingTime;
    }
}
