﻿using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MoreMountains.TopDownEngine
{
    public class Armor : MonoBehaviour
    {
        /// the name of the weapon, only used for debugging
        [Tooltip("the name of the weapon, only used for debugging")]
        public string ArmorName;
        /// the name of the inventory item corresponding to this weapon. Automatically set (if needed) by InventoryEngineWeapon
        public string ArmorID { get; set; }
        /// the weapon's owner
        public Character Owner { get; protected set; }
        /// the weapon's owner's CharacterHandleWeapon component
        public CharacterArmorHandler CharacterHandleArmor { get; set; }


        [Header("Settings")]
        /// If this is true, the weapon will initialize itself on start, otherwise it'll have to be init manually, usually by the CharacterHandleWeapon class
		[Tooltip("If this is true, the weapon will initialize itself on start, otherwise it'll have to be init manually, usually by the CharacterHandleWeapon class")]
        public bool InitializeOnStart = false;

        [Header("Position")]
        /// an offset that will be applied to the armor once attached to the center of the ArmorAttachment transform.
        [Tooltip("an offset that will be applied to the armor once attached to the center of the ArmorAttachment transform.")]
        public Vector3 ArmorAttachmentOffset = Vector3.zero;
        /// should that weapon be flipped when the character flips?
        [Tooltip("should that weapon be flipped when the character flips?")]
        public bool FlipArmorOnCharacterFlip = true;
        /// the FlipValue will be used to multiply the model's transform's localscale on flip. Usually it's -1,1,1, but feel free to change it to suit your model's specs
        [Tooltip("the FlipValue will be used to multiply the model's transform's localscale on flip. Usually it's -1,1,1, but feel free to change it to suit your model's specs")]
        public Vector3 RightFacingFlipValue = new Vector3(1, 1, 1);
        /// the FlipValue will be used to multiply the model's transform's localscale on flip. Usually it's -1,1,1, but feel free to change it to suit your model's specs
        [Tooltip("the FlipValue will be used to multiply the model's transform's localscale on flip. Usually it's -1,1,1, but feel free to change it to suit your model's specs")]
        public Vector3 LeftFacingFlipValue = new Vector3(-1, 1, 1);
        /// a transform to use as the spawn point for weapon use (if null, only offset will be considered, otherwise the transform without offset)
        [Tooltip("a transform to use as the spawn point for weapon use (if null, only offset will be considered, otherwise the transform without offset)")]
        public Transform ArmorUseTransform;

        /// if true, the weapon is flipped
        [MMReadOnly]
        [Tooltip("if true, the weapon is flipped right now")]
        public bool Flipped;


        protected CharacterMovement _characterMovement;
        protected TopDownController _controller;
        protected Animator _ownerAnimator;
        protected SpriteRenderer _spriteRenderer;
        protected float _movementMultiplierStorage = 1f;
        protected Vector3 _ArmorOffset;
        protected Vector3 _ArmorAttachmentOffset;
        protected Transform _ArmorAttachment;
        protected List<int> _ownerAnimatorParameters;

        protected virtual void Start()
        {
            if (InitializeOnStart)
            {
                Initialization();
            }
        }

        public virtual void Initialization()
        {
            Flipped = false;
            _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
            InitializeAnimatorParameters();
        }

        /// <summary>
        /// Sets the Armor's owner
        /// </summary>
        /// <param name="newOwner">New owner.</param>
        public virtual void SetOwner(Character newOwner, CharacterArmorHandler handleArmor)
        {
            Owner = newOwner;
            if (Owner != null)
            {
                CharacterHandleArmor = handleArmor;
                _characterMovement = Owner.GetComponent<CharacterMovement>();
                _controller = Owner.GetComponent<TopDownController>();

                if (CharacterHandleArmor.AutomaticallyBindAnimator)
                {
                    if (CharacterHandleArmor.CharacterAnimator != null)
                    {
                        _ownerAnimator = CharacterHandleArmor.CharacterAnimator;
                    }
                    if (_ownerAnimator == null)
                    {
                        _ownerAnimator = CharacterHandleArmor.gameObject.MMGetComponentNoAlloc<Character>().CharacterAnimator;
                    }
                    if (_ownerAnimator == null)
                    {
                        _ownerAnimator = CharacterHandleArmor.gameObject.MMGetComponentNoAlloc<Animator>();
                    }
                }
            }
        }

        public virtual void InitializeAnimatorParameters()
        {
            if (_ownerAnimator != null)
            {
                _ownerAnimatorParameters = new List<int>();
            }
        }
    }
}
