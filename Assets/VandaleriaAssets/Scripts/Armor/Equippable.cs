﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MoreMountains.TopDownEngine
{
    public class Equippable : MonoBehaviour
    {
        /// the name of the weapon, only used for debugging
        [Tooltip("the name of the weapon, only used for debugging")]
        public string EquippableName;
        /// the name of the inventory item corresponding to this weapon. Automatically set (if needed) by InventoryEngineWeapon
        public string EquippableID { get; set; }
        /// the weapon's owner
        public Character Owner { get; protected set; }
        /// the weapon's owner's CharacterHandleWeapon component
        public CharacterEquippableHandler CharacterHandleEquippable  { get; set; }
        /// an offset that will be applied to the armor once attached to the center of the ArmorAttachment transform.
        [Tooltip("an offset that will be applied to the armor once attached to the center of the ArmorAttachment transform.")]
        public Vector3 EquippableAttachmentOffset = Vector3.zero;

        [Header("Settings")]
        /// If this is true, the weapon will initialize itself on start, otherwise it'll have to be init manually, usually by the CharacterHandleWeapon class
		[Tooltip("If this is true, the weapon will initialize itself on start, otherwise it'll have to be init manually, usually by the CharacterHandleWeapon class")]
        public bool InitializeOnStart = false;

        protected TopDownController _controller;
        protected SpriteRenderer _spriteRenderer;
        protected Vector3 _EquippableOffset;
        protected Vector3 _EquippableAttachmentOffset;
        protected Transform _EquippableAttachment;


        protected virtual void Start()
        {
            if (InitializeOnStart)
            {
                Initialization();
            }
        }

        public virtual void Initialization()
        {
            _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        }


        /// <summary>
        /// Sets the Armor's owner
        /// </summary>
        /// <param name="newOwner">New owner.</param>
        public virtual void SetOwner(Character newOwner, CharacterEquippableHandler handleEquippable)
        {
            Owner = newOwner;
            if (Owner != null)
            {
                CharacterHandleEquippable = handleEquippable;
                _controller = Owner.GetComponent<TopDownController>();
            }
        }
    }
}