﻿using UnityEngine;
using MoreMountains.Tools;
using System;
using Cinemachine;

namespace MoreMountains.TopDownEngine
{
    /// <summary>
    /// A class meant to be used in conjunction with an object pool (simple or multiple)
    /// to spawn objects based on the current number of objects spawned in without 
    /// </summary>
    [AddComponentMenu("TopDown Engine/Character/AI/Automation/EnemySpawner")]

	public class EnemySpawnManager : MonoBehaviour
	{
		/// whether or not this spawner can spawn
		[Tooltip("whether or not this spawner can spawn")]
		public bool CanSpawn = true;

		///the maximum number of enemies that should exist in the world from this spawner
		public int maxSpawnedEnemies;
		protected int currentPoolCount = 0;

		/// the object pooler associated to this spawner
		public MMObjectPooler ObjectPooler { get; set; }

		/// the area that enemies are allowed to spawn in
		public BoxCollider2D spawningBounds;

		/// used to determine where the player can see to prevent enemies spawning where the player can see
		//public CinemachineBrain virtualCamera;


		// Start is called before the first frame update
		protected void Start()
		{
			Initialization();
			//ObjectPooler.FillObjectPool();
			
		}

		protected virtual void Initialization()
		{
			if (GetComponent<MMMultipleObjectPooler>() != null)
			{
				ObjectPooler = GetComponent<MMMultipleObjectPooler>();
			}
			if (GetComponent<MMSimpleObjectPooler>() != null)
			{
				ObjectPooler = GetComponent<MMSimpleObjectPooler>();
			}
			if (ObjectPooler == null)
			{
				Debug.LogWarning(this.name + " : no object pooler (simple or multiple) is attached to this Projectile Weapon, it won't be able to shoot anything.");
				return;
			}
			
		}

		// && ActiveSpawnedObjects() < maxSpawnedEnemies

		protected virtual void Update()
		{
            if (AtMaxSpawnedEnemies())
            {
				if (CanSpawn)
				{
					Spawn();
				}
			}
			
		}

		protected virtual bool AtMaxSpawnedEnemies()
        {
			if (currentPoolCount >= maxSpawnedEnemies && CanSpawn)
			{
				TurnSpawnOff();
				return false;
			}
			return true;
		}


		protected virtual void Spawn()
		{
			GameObject nextGameObject = ObjectPooler.GetPooledGameObject();
			Vector2 spawnLocation = new Vector2(UnityEngine.Random.Range(spawningBounds.bounds.min.x, spawningBounds.bounds.max.x), UnityEngine.Random.Range(spawningBounds.bounds.min.y, spawningBounds.bounds.max.y));
			//Vector2 CameraSpace = new Vector2(virtualCamera.OutputCamera.MMCameraWorldSpaceWidth(), virtualCamera.OutputCamera.MMCameraWorldSpaceHeight());
			//bool isPlaced = false;

			// mandatory checks
			if (nextGameObject == null) { return; }
			if (nextGameObject.GetComponent<MMPoolableObject>() == null)
			{
				throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
			}

			// we activate the object
			nextGameObject.gameObject.SetActive(true);
			nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();


			// we check if our object has an Health component, and if yes, we revive our character
			Health objectHealth = nextGameObject.gameObject.MMGetComponentNoAlloc<Health>();
			if (objectHealth != null)
			{
				objectHealth.Revive();
			}

			/*while (!isPlaced)
			{
				Vector2 spawnLocation = new Vector2(UnityEngine.Random.Range(spawningBounds.bounds.min.x, spawningBounds.bounds.max.x), UnityEngine.Random.Range(spawningBounds.bounds.min.y, spawningBounds.bounds.max.y));
				// check if the random location is oustide the camera bounds
				if ((spawnLocation.x <= CameraSpace.x && spawnLocation.x >= CameraSpace.x) && (spawnLocation.y <= CameraSpace.y && spawnLocation.y >= CameraSpace.y))
				{
					nextGameObject.transform.position = spawnLocation;
					isPlaced = true;
				}
			}*/
			nextGameObject.transform.position = spawnLocation;
			//&& ObjectPooler.GetComponent<MMSimpleObjectPooler>().PoolSize != 0
			if (nextGameObject.activeInHierarchy)
			{
				currentPoolCount++;
			}

			
		}

		/// <summary>
		/// Toggles spawn on and off
		/// </summary>
		public virtual void ToggleSpawn()
		{
			CanSpawn = !CanSpawn;
		}

		/// <summary>
		/// Turns spawning off
		/// </summary>
		public virtual void TurnSpawnOff()
		{
			CanSpawn = false;
		}

		/// <summary>
		/// Turns spawning on
		/// </summary>
		public virtual void TurnSpawnOn()
		{
			CanSpawn = true;
		}
	}
}
