﻿using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


namespace MoreMountains.TopDownEngine
{
    [CreateAssetMenu(fileName = "InventoryArmor", menuName = "MoreMountains/TopDownEngine/Inventory Armor", order = 2)]
    [Serializable]
    /// <summary>
    /// Armor item in the TopDown Engine
    /// </summary>
    public class InventoryArmor : InventoryItem
    {
        public enum AutoEquipModes { NoAutoEquip, AutoEquipIfHigherArmor, AutoEquipIfUnarmored }

        [Header("Armor")]
        [MMInformation("Here you need to bind the armor you want to equip when picking that item.", MMInformationAttribute.InformationType.Info, false)]
        /// the armor to equip
        [Tooltip("the armor to equip")]
        public Armor EquippableArmor;
        /// how to equip this weapon when picked : not equip it, automatically equip it, or only equip it if no weapon is currently equipped
        [Tooltip("how to equip this weapon when picked : not equip it, automatically equip it if the new armor is better, or only equip it if no weapon is currently equipped")]
        public AutoEquipModes AutoEquipMode = AutoEquipModes.AutoEquipIfUnarmored;

        /// <summary>
		/// When we grab the armor, we equip it
		/// </summary>
        public override bool Equip()
        {   
            EquipArmor(EquippableArmor);
            return true;
        }

        /// <summary>
        /// When dropping or unequipping a armor, we remove it
        /// </summary>
        public override bool UnEquip()
        {
            // if this is a currently equipped weapon, we unequip it
            if (this.TargetEquipmentInventory == null)
            {
                return false;
            }

            if (this.TargetEquipmentInventory.InventoryContains(this.ItemID).Count > 0)
            {
                EquipArmor(null);
            }

            return true;
        }

        /// <summary>
        /// Grabs the CharacterArmorHandler component and sets the Armor
        /// </summary>
        /// <param name="newArmor">New Armor.</param>
        public virtual void EquipArmor(Armor newArmor)
        {
            if (EquippableArmor == null)
            {
                return;
            }
            if (TargetInventory.Owner == null)
            {
                return;
            }
            CharacterArmorHandler characterHandleArmor = TargetInventory.Owner.GetComponent<CharacterArmorHandler>();
            if (characterHandleArmor != null)
            {
                characterHandleArmor.ChangeArmor(newArmor, this.ItemID);
            }
        }
    }
}