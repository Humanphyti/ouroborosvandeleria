﻿using MoreMountains.Tools;
using MoreMountains.InventoryEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.TopDownEngine;

namespace MoreMountains.InventoryEngine
{
    [CreateAssetMenu(fileName = "MPBonusItem", menuName = "MoreMountains/TopDownEngine/MP Bonus Item", order = 2)]
    [Serializable]
    public class MPBonusItem : InventoryItem
    {
		[Header("MP Bonus")]
		/// the amount of health to add to the player when the item is used
		public int MPBonus;
		/// <summary>
		/// What happens when the object is used 
		/// </summary>
		public override bool Use()
		{
			base.Use();
			if(TargetInventory.Owner == null)
            {
				return false;
            }
			MagicPower characterMP = TargetInventory.Owner.GetComponent<MagicPower>();
			if (characterMP == null)
			{
				characterMP.GetMP(MPBonus, TargetInventory.gameObject);
				return true;
			}
			else
			{
				Debug.LogFormat("increase character's MP by " + MPBonus);
				return true;
			}
		}
	}
}