﻿using MoreMountains.Tools;
using MoreMountains.InventoryEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.TopDownEngine;

namespace MoreMountains.InventoryEngine
{
	[CreateAssetMenu(fileName = "StaminaBonusItem", menuName = "MoreMountains/TopDownEngine/Stamina Bonus Item", order = 3)]
	[Serializable]
	/// <summary>
	/// Pickable health item
	/// </summary>
	public class StaminaBonusItem : InventoryItem
    {
		[Header("Stamina Bonus")]
		//[MMInformation("Here you need specify the amount of Stamina gained when using this item.", MMInformationAttribute.InformationType.Info, false)]
		/// the amount of health to add to the player when the item is used
		[Tooltip("the amount of stamina to add to the player when the item is used")]
		public int StaminaBonus;

		/// <summary>
		/// When the item is used, we try to grab our character's Health component, and if it exists, we add our health bonus amount of health
		/// </summary>
		public override bool Use()
		{
			base.Use();

			if (TargetInventory.Owner == null)
			{
				return false;
			}

			Stamina characterStamina = TargetInventory.Owner.GetComponent<Stamina>();
			if (characterStamina != null)
			{
				characterStamina.GetStamina(StaminaBonus, TargetInventory.gameObject);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
